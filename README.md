#php

####Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with php](#setup)
    * [What php affects](#what-php-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with php](#beginning-with-php)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

##Overview

The php module installs PHP 5.5.

##Setup

###What php affects
* php package.
* php configuration files.
* php repo webtatic.

###Beginning with php

```puppet
include php
```

##Usage

All interaction with the php module can do be done through the main php class.

###I just want php, what's the minimum I need?

```puppet
include php
```

##Reference

###Classes

####Public Classes

* php: Main class, includes all other classes.

####Private Classes

* php::package: Handles the snmpd package install.
* php::config: Handles the configuration files.
* php::repo: Handles the repo for installing PHP 5.5.

###Parameters for php

The following parameters are available in the php module:

####There are currently no paramters for this class.

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5

Support on other platforms has not been tested and is currently not supported. 

##Development

This module needs to have hard coded versions for our infrastructure.

###Contributors

Mike Travis - <mtravis@webcourseworks.com>


