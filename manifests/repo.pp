# Class: php::repo
#
# This class manages the php repo - webtatic.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly. 
class php::repo {

  file { '/etc/yum.repos.d/webtatic.repo':
    ensure => present,
    mode   => '0644',
    source => 'puppet:///modules/php/webtatic.repo',
  }

}