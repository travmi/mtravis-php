# == Class: php
#
# This module manges the installation of php 5.5 and extensions.
#
# === Parameters
#
# There are currently no paramters for this module.
#
# === Authors
#
# Author Name <mtravis@webcourseworks.com>
#
# === Copyright
#
# Copyright 2014 Mike Travis, unless otherwise noted.
#
class php (


){

  anchor { 'php::begin': } ->
    class  { '::php::repo': } ->
    class  { '::php::package': } ->
    class  { '::php::config': }
  anchor { 'php::end': }

}
