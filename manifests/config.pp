# == Class: php::config
#
# This class manages the PHP config files.
# 
# Parameters
# There are no parameters called from this class.
#
# === Authors
#
# Author Name: mtravis@webcourseworks.com
#
# === Copyright
#
# Copyright 2014 Web Courseworks Ltd.
class php::config {

  file { '/etc/httpd/conf.d/php.conf':
   path    => '/etc/httpd/conf.d/php.conf',
   ensure  => 'file',
   content => template('php/php.conf.erb'),
  }

 file { '/etc/php.ini':
  path    => '/etc/php.ini',
  ensure  => 'file',
  replace => true,
  content => template('php/php.ini.erb'),
  mode    => '777',
 }

# file { '/etc/php.d':
#  path    => '/etc/php.d',
#  ensure  => 'directory',
#  source  => 'puppet:///modules/php/php.d',
#  recurse => true,
#  replace => true,
# }

# file { '/etc/php-zts.d':
#  path    => '/etc/php-zts.d',
#  ensure  => 'directory',
#  source  => 'puppet:///modules/php/php.d',
#  recurse => true,
#  replace => true,
# }

}
