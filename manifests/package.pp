# Class: php::package
#
# This module manages the packages required for WCW for php.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly. 
class php::package {

  package { 'php55w':
    ensure => 'latest',
   }
 
  package { 'php55w-cli':
    ensure => 'latest',
  }
  
  package { 'php55w-common':
    ensure => 'latest',
  }
  
  package { 'php55w-devel':
    ensure => 'latest',
  }
  
  package { 'php55w-gd':
    ensure => 'latest',
  }
  
  package { 'php55w-intl':
    ensure => 'latest',
  }
  
  package { 'php55w-mbstring':
    ensure => 'latest',
  }
  
  package { 'php55w-mcrypt':
    ensure => 'latest',
  }
  
  package { 'php55w-mysql':
    ensure => 'latest',
  }
  
  package { 'php55w-opcache':
    ensure => 'latest',
  }
  
  package { 'php55w-pdo':
    ensure => 'latest',
  }
  
  package { 'php55w-pear':
    ensure => 'latest',
  }
  
  package { 'php55w-pgsql':
    ensure => 'latest',
  }
  
  package { 'php55w-soap':
    ensure => 'latest',
  }
  
  package { 'php55w-xml':
    ensure => 'latest',
  }

}